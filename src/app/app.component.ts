import { Component, ViewChild } from '@angular/core';
import { MatSidenav, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    SMALL_WIDTH_BREAKPOINT = 720;


    @ViewChild(MatSidenav) sidenav: MatSidenav;

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    title = 'reconhecimento-geografico-front-end';

    constructor(private breakpointObserver: BreakpointObserver,
        private router: Router,
        private matDialog: MatDialog) {

        this.router.events.subscribe(() => {
            
                if (this.sidenav) this.sidenav.opened = false;
            
        });
    }

    isScreenSmall(): boolean {
        return window.matchMedia(`(max-width: ${this.SMALL_WIDTH_BREAKPOINT}px)`).matches;
    }

    isExpanded = true;
    showSubmenu: boolean = false;
    isShowing = true;
    showSubSubMenu: boolean = false;
    isExpandedSeguranca = true;
    showSubmenuSeguranca: boolean = false;
    isShowingSeguranca = true;
    showSubSubMenuSeguranca: boolean = false;

    mouseenter() {
        if (!this.isExpanded) {
            this.isShowing = true;
        }
        if (!this.isExpandedSeguranca) {
            this.isShowingSeguranca = true;
        }
    }

    mouseleave() {
        if (!this.isExpanded) {
            this.isShowing = false;
        }
        if (!this.isExpandedSeguranca) {
            this.isShowingSeguranca = false;
        }

    }

}
