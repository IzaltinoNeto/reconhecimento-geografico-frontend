import { WebDataRocksPivot } from './../webdatarocks/webdatarocks.angular4';
import  ListaDeTrabalho  from 'src/app/model/listaDeTrabalho';
import { HttpClient } from '@angular/common/http';

import { Injectable, Component } from '@angular/core';
import Territorio from '../model/territorio';
import { delay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class RelatorioService {
    dataReport = [];
    baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = 'http://localhost:8080/';
    }

    getRelatorios(component) {

        this.http.get(this.baseURL+'item-lista?listaId=36')
            .subscribe((data: any) => {
                console.log("entrou na requisição");
                //montar um vetor de objetos com os dados necessarios
                data.content.forEach((element, index) => {

                    console.log('elemento: ', element);
                    this.dataReport.push({

                        "imovel": element.imovel.logradouro + ' - ' + element.imovel.numero,
                        "territorio": element.imovel.territorio.nome,
                        "territorio Pai": element.imovel.territorio.territorioPai ?
                            element.imovel.territorio.territorioPai.nome : '',
                        "tipo": element.imovel.tipoImovel.nome,
                        "Categoria": element.imovel.territorio.categoria.nome,
                        "zona": element.imovel.territorio.zona,
                        "complemento": element.imovel.complemento,
                        "Quantidade de Habitantes": element.reconhecimento.conteudo.question1,
                        "Quantidade de Familias": element.reconhecimento.conteudo.question2,
                        "Numero de Armadilhas instaladas": element.reconhecimento.conteudo.question3,
                        "Numero de Caixas Dagua desprotegidas": element.reconhecimento.conteudo.question5,
                        "Outros depositos desprotegidos": element.reconhecimento.conteudo.question4

                    });

                }
                );
                this.refreshReport(component);
            });


    }

    getRelatoriosPorLista(component, listas: ListaDeTrabalho[]) {
        

        console.log("entrou na requisição: ", listas);
        //montar um vetor de objetos com os dados necessarios
        listas.forEach((lista, index) => {
            lista.itens.forEach(element => {
                console.log('elemento: ', element);
                this.dataReport.push({

                    "imovel": element.imovel.logradouro + ' - ' + element.imovel.numero,
                    "territorio": element.imovel.territorio.nome,
                    "territorio Pai": element.imovel.territorio.territorioPai ?
                        element.imovel.territorio.territorioPai.nome : '',
                    "tipo": element.imovel.tipoImovel.nome,
                    "Categoria": element.imovel.territorio.categoria.nome,
                    "zona": element.imovel.territorio.zona,
                    "complemento": element.imovel.complemento,
                    "Quantidade de Habitantes": element.reconhecimento.conteudo.question1,
                    "Quantidade de Familias": element.reconhecimento.conteudo.question2,
                    "Numero de Armadilhas instaladas": element.reconhecimento.conteudo.question3,
                    "Numero de Caixas Dagua desprotegidas": element.reconhecimento.conteudo.question5,
                    "Outros depositos desprotegidos": element.reconhecimento.conteudo.question4

                });
            });


        }


        );
       
     


    }
    refreshReport(component){
        component.child.webDataRocks.setReport(component.getReport());
        component.child.webDataRocks.refresh();
    }

    
    getRelatoriosPorTerritorio(component, territorio: Territorio) {


        this.http.get(this.baseURL+`item-lista/byTerritorio?territorioId=${territorio.id}`)
        .subscribe((data: any) => {
            console.log("entrou na requisição");
            //montar um vetor de objetos com os dados necessarios
            data.content.forEach((element, index) => {

                console.log('elemento: ', element);
                this.dataReport.push({

                    "imovel": element.imovel.logradouro + ' - ' + element.imovel.numero,
                    "territorio": element.imovel.territorio.nome,
                    "territorio Pai": element.imovel.territorio.territorioPai ?
                        element.imovel.territorio.territorioPai.nome : '',
                    "tipo": element.imovel.tipoImovel.nome,
                    "Categoria": element.imovel.territorio.categoria.nome,
                    "zona": element.imovel.territorio.zona,
                    "complemento": element.imovel.complemento,
                    "Quantidade de Habitantes": element.reconhecimento.conteudo.question1,
                    "Quantidade de Familias": element.reconhecimento.conteudo.question2,
                    "Numero de Armadilhas instaladas": element.reconhecimento.conteudo.question3,
                    "Numero de Caixas Dagua desprotegidas": element.reconhecimento.conteudo.question5,
                    "Outros depositos desprotegidos": element.reconhecimento.conteudo.question4

                });

            }
            );
            //permite a inicialização do component de relatorio na tela           
            component.child.webDataRocks.setReport(component.getReport());
            component.child.webDataRocks.refresh();
        });


    }



}