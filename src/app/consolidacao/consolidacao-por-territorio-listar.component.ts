import { Location } from '@angular/common';

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, tap, finalize } from 'rxjs/operators';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Page } from '../model/Page';
import Territorio from '../model/territorio';
import { HttpTerritorioService } from '../territorio/service/http-territorio.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';
import { HttpListaDeTrabalhoService } from '../lista-de-trabalho/service/http-lista-de-trabalho.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
    selector: 'consolidacao-por-territorio-listar',
    templateUrl: './consolidacao-por-territorio-listar.component.html',
    styleUrls: ['./consolidacao.component.scss']
})
export class ConsolidacaoPorTerritorioListarComponent implements OnInit {

    //Atributos para selecao de filtro por imovel Pai
    formGroup: FormGroup = this.formBuilder.group({
        
    });
    mostrarRelatorio : boolean = false;
    //Atributos para tabela
    colunasTabela: string[] = ['select','territorio'];
    dadosTabela: Territorio[] = [];
    selectionModel = new SelectionModel<Territorio>(true, []);
    quantidadeRegistros = 0;
    carregandoResultados = true;
    tempoLimiteAtingido = false;

    filtro: string = "";
    tamanhoMinimoTextoFiltro: number = 2;
    dataSource : any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private apiTerritorioService: HttpTerritorioService,
        private formBuilder: FormBuilder,
        private location : Location
    ) { this.mostrarRelatorio = false;}

    ngOnInit() {
        this.configurarTabela();
        this.dataSource = new MatTableDataSource<Territorio>([]);
    }

    configurarTabela() {
        // Se o imovel muda a ordem na tabela, volta para primeira pagina
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.carregandoResultados = true;
                        return this.apiTerritorioService.pesquisarComFiltro(
                            null,
                            this.filtro,
                            this.sort.active,
                            this.sort.direction,
                            this.paginator.pageIndex,
                            this.paginator.pageSize | 30
                        );
                    
                }),
                map((resposta: Page<Territorio>) => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = false;
                    this.quantidadeRegistros = resposta.totalElements;
                    return resposta.content;
                }),
                catchError(() => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = true;
                    return observableOf([]);
                })
            ).subscribe(resposta =>{ 
                console.log('dados da tabela: ', resposta);
                this.dadosTabela = resposta;
                this.exibirCheckbox();});
    }

    aplicarFiltroTabela() {
        if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
            this.paginator.page.emit();
        } else {
            this.filtro = "";
        }

        this.filtrarListas();
    }

    limparFiltroTabela() {
        this.filtro = "";
        this.paginator.page.emit();
        
    }

    aoDigitarFiltroTabela(filtro: string) {
        this.filtro = filtro;
        if (this.filtro.length === 0) {
            this.paginator.page.emit();
        }
    }

    selecionar(row){
        this.selectionModel.clear();
        this.selectionModel.toggle(row);
    }
    
      exibirCheckbox() {
        console.log("Cheguei")
        this.dataSource.data = this.dadosTabela;
    
        this.selectionModel = new SelectionModel<Territorio>(true, []);
      }
    
      /** Se o número de elementos selecionados corresponde ao número total de linhas. */
      todosElementosEstaoSelecionados() {
    
        const numeroElementosSelecionados = this.selectionModel.selected.length;
        const numeroTotalElementos = this.dataSource.data.length;
        return numeroElementosSelecionados === numeroTotalElementos;
      }
    
      /** Selects all rows if they are not all selected; otherwise clear selection. */
      masterToggle() {
        this.todosElementosEstaoSelecionados() ?
          this.selectionModel.clear() :
          this.selectionModel.clear();
      }
    

    

    filtrarListas() {
        this.paginator.page.emit();
    }

    limparFiltroTerritorioSelecionada() {
        this.formGroup.controls['territorio'].setValue(null);
        this.paginator.page.emit();
    }

    consolidar() {
        this.selectionModel.selected.forEach(element => {
          console.log('element: ', element);
        });
        this.mostrarRelatorio = true;
      }

      voltar(){
        if(this.mostrarRelatorio)
            this.mostrarRelatorio = false;
        else
            this.location.back();
    }
}
