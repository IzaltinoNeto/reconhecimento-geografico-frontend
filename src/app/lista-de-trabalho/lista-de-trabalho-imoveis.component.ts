import { HttpFormularioService } from './../service-generico/http-formulario.service';
import ListaDeTrabalho  from 'src/app/model/listaDeTrabalho';
import { HttpImovelService } from './../imovel/service/http-imovel.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { catchError, map, switchMap, debounceTime, tap, finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { SelectionModel } from '@angular/cdk/collections';

import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import ItemLista from '../model/itemLista';
import Imovel from '../model/imovel';
import { HttpItemListaService } from './service/http-item-lista.service';
import ItemListaId from '../model/itemListaId';
import { Formulario } from '../model/Formulario';
import ReconhecimentoGeografico from '../model/reconhecimentoGeografico';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';

@Component({
    selector: 'app-lista-de-trabalho-imoveis.',
    templateUrl: './lista-de-trabalho-imoveis.component.html',
    styleUrls: ['./lista-de-trabalho.component.scss']
})

export class ListaDeTrabalhoImoveisComponent implements OnInit {

    dataSource;
    displayedColumns: string[] = ['select', 'logradouro'];
    selectionModel = new SelectionModel<ItemLista>(true, []);

    @Input() listaDeTrabalho: ListaDeTrabalho;
    itens: ItemLista[] = [];
    formGroup: FormGroup = this.formBuilder.group({
        "imovel": new FormControl("")
    });
    imoveisFiltrados: Imovel[] = [];
    isLoadingImovel: boolean = false;
    imovelSelecionado: Imovel;
    formulario: Formulario;

    constructor(private apiItemListaService: HttpItemListaService, 
        private apiFormularioService: HttpFormularioService, 
        private route: ActivatedRoute,
        private matDialog: MatDialog,
        private apiImovelService: HttpImovelService,
        private dialog: MatDialog,
        public matSnackBar: MatSnackBar, private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private router : Router) {

    }

    ngOnInit() {
        this.pesquisarFormulario();
        this.pesquisarTodosOsImoveis();
        this.pesquisarImoveisDaLista();
        this.dataSource = new MatTableDataSource<ItemLista>([]);
    }

    pesquisarTodosOsImoveis() {
        console.log("this.formGroup.value", this.formGroup.value)
        this.formGroup
            .get('imovel')
            .valueChanges
            .pipe(
                debounceTime(300),
                tap(() => this.isLoadingImovel = true),
                switchMap(value => this.apiImovelService.pesquisarTodos()
                    .pipe(
                        finalize(() => this.isLoadingImovel = false),
                    )
                )
            )
            .subscribe((imoveis: Imovel[]) => {
                this.imoveisFiltrados = imoveis;               
                
            });

        this.formGroup.get("imovel").setValue(this.formGroup.value.imovel);
    }

    selecionarImovel(event: any) {
        this.imovelSelecionado = event.option.value;
    }

    checarSelecaoImovel() {
        if (!this.imovelSelecionado || this.imovelSelecionado !== this.formGroup.controls['imovel'].value) {
            this.formGroup.controls['imovel'].setValue(null);
            this.imovelSelecionado = null;
        }
    }

    displayFnImovel(imovel: Imovel) {
        if (imovel) { return imovel.logradouro+', '+imovel.numero; }
    }

    exibirCheckbox() {
        console.log("Cheguei")
        this.dataSource.data = this.itens;
        this.selectionModel = new SelectionModel<ItemLista>(true, []);
    }

    /** Se o número de elementos selecionados corresponde ao número total de linhas. */
    todosElementosEstaoSelecionados() {

        const numeroElementosSelecionados = this.selectionModel.selected.length;
        const numeroTotalElementos = this.dataSource.data.length;
        return numeroElementosSelecionados === numeroTotalElementos;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.todosElementosEstaoSelecionados() ?
            this.selectionModel.clear() :
            this.dataSource.data.forEach(row => this.selectionModel.select(row));
    }
    pesquisarFormulario() {
        
        this.apiFormularioService.pesquisarPorId('1').pipe(
            map((data: Formulario) => { this.formulario = data; console.log('data', data); }),
            catchError(err => this.tratarErroAoPesquisarItemLista(err))).subscribe();
    }


    pesquisarImoveisDaLista() {
        
        this.apiItemListaService.pesquisarPorListaId(this.listaDeTrabalho.id, null, null, 0, 100).pipe(
            map((data) => { this.tratarSucessoAoPesquisarItemLista(data); console.log('data', data); }),
            catchError(err => this.tratarErroAoPesquisarItemLista(err))).subscribe();
    }

    tratarSucessoAoPesquisarItemLista(data): any {
        this.itens = data.content;
        this.exibirCheckbox();
    }

    tratarErroAoPesquisarItemLista(err: any): any {
        console.log("Erro", err);
    }
    adicionarImovelALista() {
        this.apiFormularioService.pesquisarPorId('1').subscribe(formulario => {

            let reconhecimento = 
                new ReconhecimentoGeografico('Reconhecimento Geografico',
                                             null,
                                             formulario);

            let itemListaId = new ItemListaId(this.imovelSelecionado.id,
                this.listaDeTrabalho.id);

            let itemLista = new ItemLista(  itemListaId,
                                            this.imovelSelecionado,
                                            this.listaDeTrabalho,
                                            reconhecimento);
            console.log('Item Lista a ser cadastrado: ', itemLista);

            this.apiItemListaService.salvarItemListaInteiro(itemLista).pipe(
                map((data: ItemLista) => this.tratarSucessoAoAdicionar(data)),
                catchError(err => this.tratarErroAoAdicionar(err))).subscribe();
        })
        
        
    }
    tratarSucessoAoAdicionar(data) {
        this.pesquisarTodosOsImoveis();
        this.pesquisarImoveisDaLista();

    }

    tratarErroAoAdicionar(erro): any {
        this.abrirSnackBar("Erro ao Adicionar! " + erro.error, null, "red-snackbar")
    }

    removerImovelDaLista() {
        
        this.selectionModel.selected.forEach(element => {
            console.log('element: ', element);
            this.apiItemListaService.removerImovelDaLista(element).pipe(
                map((data: ItemLista) => this.tratarSucessoAoAdicionar(data)),
                catchError(err => this.tratarErroAoAdicionar(err))).subscribe();
        });
    }

    tratarErroAoRemover(erro): any {
        this.abrirSnackBar("Erro ao remover! " + erro.error, null, "red-snackbar")
    }

    concluir(){
        const dialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: {titulo: "Informação", conteudo: "Agora você pode acessar essa lista pelo aplicativo."}
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
           this.router.navigate(['listas-de-trabalho']);
        });
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }
}
