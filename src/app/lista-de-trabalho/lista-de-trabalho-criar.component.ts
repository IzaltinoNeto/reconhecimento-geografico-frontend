import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import { FormGroup} from '@angular/forms';
import { ListaDeTrabalhoFormService } from './service/lista-de-trabalho-form.service';
import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';

/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-lista-de-trabalho-criar',
    templateUrl: './lista-de-trabalho-criar.component.html',
    styleUrls: ['./lista-de-trabalho.component.scss']
})

export class ListaDeTrabalhoCriarComponent implements OnInit {

    formGroup: FormGroup;
    
    listaDeTrabalho: ListaDeTrabalho;

    constructor(
        private apiDataService: HttpListaDeTrabalhoService,
        private router: Router,
        public snackBar: MatSnackBar,
        private dialog: MatDialog,     
        private listaDeTrabalhoFormService: ListaDeTrabalhoFormService
        ) {
    }

    ngOnInit() {
        this.formGroup = this.listaDeTrabalhoFormService.getNewFormGroup();
        this.listaDeTrabalho = new ListaDeTrabalho();  
    }

    salvar() {
        console.log("Salvar ListaDeTrabalho!", this.formGroup.value);

        const dialogoConfirmacao = this.dialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Salvar", conteudo: "Confirma criação?" }
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                ListaDeTrabalho.popularBaseadoEmFormGroup(this.listaDeTrabalho, this.formGroup);
                this.apiDataService.salvar(this.listaDeTrabalho).pipe(
                    map((data: ListaDeTrabalho) => this.onSuccess(data)),
                    catchError(err => this.handleError(err))).subscribe();
            }
        });
    }

    onSuccess(data) {
        console.log("Tudo OK!", data)
        this.abrirSnackBar("ListaDeTrabalho cadastrado com sucesso!", null, "green-snackbar")
        this.router.navigate([`lista-de-trabalho-editar/${data.id}`]);  
    }

    handleError(erro): any {
        console.log("Erro", erro)
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
