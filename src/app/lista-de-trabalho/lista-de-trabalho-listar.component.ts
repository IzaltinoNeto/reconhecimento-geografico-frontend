import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, tap, finalize } from 'rxjs/operators';

import { MatPaginator, MatSort } from '@angular/material';
import { Page } from '../model/Page';
import Territorio from '../model/territorio';
import { HttpTerritorioService } from '../territorio/service/http-territorio.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';

@Component({
    selector: 'app-lista-de-trabalho-listar',
    templateUrl: './lista-de-trabalho-listar.component.html',
    styleUrls: ['./lista-de-trabalho.component.scss']
})
export class ListaDeTrabalhoListarComponent implements OnInit {

    //Atributos para selecao de filtro por imovel Pai
    formGroup: FormGroup = this.formBuilder.group({
        
    });

    //Atributos para tabela
    colunasTabela: string[] = ['listaDeTrabalho'];
    dadosTabela: ListaDeTrabalho[] = [];

    quantidadeRegistros = 0;
    carregandoResultados = true;
    tempoLimiteAtingido = false;

    filtro: string = "";
    tamanhoMinimoTextoFiltro: number = 2;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private apiListaDeTrabalhoService: HttpListaDeTrabalhoService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.configurarTabela();
    }

    configurarTabela() {
        // Se o imovel muda a ordem na tabela, volta para primeira pagina
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.carregandoResultados = true;
                        return this.apiListaDeTrabalhoService.pesquisarComFiltro(
                            this.filtro,
                            this.sort.active,
                            this.sort.direction,
                            this.paginator.pageIndex,
                            this.paginator.pageSize | 30
                        );
                    
                }),
                map((resposta: Page<ListaDeTrabalho>) => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = false;
                    this.quantidadeRegistros = resposta.totalElements;
                    return resposta.content;
                }),
                catchError(() => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = true;
                    return observableOf([]);
                })
            ).subscribe(resposta =>{ 
                console.log('dados da tabela: ', resposta);
                return this.dadosTabela = resposta;});
    }

    aplicarFiltroTabela() {
        if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
            this.paginator.page.emit();
        } else {
            this.filtro = "";
        }

        this.filtrarListas();
    }

    limparFiltroTabela() {
        this.filtro = "";
        this.paginator.page.emit();
        
    }

    aoDigitarFiltroTabela(filtro: string) {
        this.filtro = filtro;
        if (this.filtro.length === 0) {
            this.paginator.page.emit();
        }
    }

    

    

    filtrarListas() {
        this.paginator.page.emit();
    }

    limparFiltroTerritorioSelecionada() {
        this.formGroup.controls['territorio'].setValue(null);
        this.paginator.page.emit();
    }

}
