import { Injectable } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';


@Injectable({
    providedIn: 'root'
})
export class ListaDeTrabalhoFormService {

    constructor(
        private formBuilder: FormBuilder
    ) {
    }
    getNewFormGroup() {
        let nome = new FormControl("", Validators.required);
        let descricao = new FormControl("");
        
        

        let formGroup = this.formBuilder.group({
            "nome": nome,
            "descricao": descricao,
            
        });
        return formGroup;
    }

}
