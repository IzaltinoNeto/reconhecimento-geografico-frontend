import { Page } from '../../model/Page';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';

import Territorio from 'src/app/model/territorio';
import ListaDeTrabalho from 'src/app/model/listaDeTrabalho';
@Injectable({
    providedIn: 'root'
})
export class HttpListaDeTrabalhoService extends HttpGenerico<ListaDeTrabalho> {

    constructor(http: HttpClient) {
        super(http, '/lista-de-trabalho')
    }

    pesquisarComFiltro(nome:string, sort: string, order: string, page: number, size: number): Observable<Page<ListaDeTrabalho>> {
        if (typeof nome !== 'string') {
            nome = "";
        }

        const href = this.urlPadrao + this.urlApi + '/byNameWithPagination';
        const requestUrl =
            `${href}?nome=${nome}&page=${page}&size=${size}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Page<ListaDeTrabalho>) => res)
            );
    }

    pesquisarComItens(nome:string, sort: string, order: string, page: number, size: number): Observable<Page<ListaDeTrabalho>> {
        if (typeof nome !== 'string') {
            nome = "";
        }

        const href = this.urlPadrao + this.urlApi + '/withItens';
        const requestUrl =
            `${href}?nome=${nome}&page=${page}&size=${size}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Page<ListaDeTrabalho>) => res)
            );
    }
   
    


}
