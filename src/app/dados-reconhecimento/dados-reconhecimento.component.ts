import { HttpImovelService } from './../imovel/service/http-imovel.service';
import { HttpTerritorioService } from './../territorio/service/http-territorio.service';
import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import Territorio from '../model/territorio';
import Imovel from '../model/imovel';

@Component({
  selector: 'app-dados-reconhecimento',
  templateUrl: './dados-reconhecimento.component.html',
  styleUrls: ['./dados-reconhecimento.component.scss']
})
export class DadosReconhecimentoComponent implements OnInit {

    resultado : string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private apiTerritorioService: HttpTerritorioService,
  private apiImovelService : HttpImovelService) { }


  ngOnInit() {
    this.resultado = "indefinido";
    if(this.data.conteudo.nome){
      this.resultado = "Territorio";
      
    }
    else{
      this.resultado = "Imovel";
    }

    console.log(this.data.conteudo);
  }

}
