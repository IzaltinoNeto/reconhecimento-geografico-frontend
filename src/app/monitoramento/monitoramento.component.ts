import { element } from 'protractor';

import { HttpCategoriaService } from '../service-generico/http-categoria.service';
import { Map } from 'ol/Map.js';
import { Component, ChangeDetectionStrategy, OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize, delay, map, catchError } from 'rxjs/operators';
import Categoria from '../model/categoria';
import Imovel from '../model/imovel';
import { MatSnackBar, MatDialog } from '@angular/material';
import TipoImovel from '../model/tipoImovel';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpTipoImovelService } from '../service-generico/http-tipo-imovel.service';
import Territorio from '../model/territorio';
import { HttpTerritorioService } from '../territorio/service/http-territorio.service';
import { ImovelFormService } from '../imovel/service/imovel-form.service';
import { HttpImovelService } from '../imovel/service/http-imovel.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import { DadosReconhecimentoComponent } from '../dados-reconhecimento/dados-reconhecimento.component';
import { HttpItemListaService } from '../lista-de-trabalho/service/http-item-lista.service';
import ItemLista from '../model/itemLista';

interface FoodNode {
    elemento: any;
    isSelected : boolean;
    children?: FoodNode[];
}



/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    elemento: any;
    level: number;
}

@Component({
    selector: 'app-monitoramento',
    templateUrl: './monitoramento.component.html',
    styleUrls: ['./monitoramento.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoramentoComponent implements OnInit, AfterViewInit {
    @ViewChild("tree") mattree;
    @ViewChild("mapa") mapa;
    irmaos: Observable<Imovel[]>;
    irmaosSubject: BehaviorSubject<Imovel[]>;

    territorios: Observable<Territorio[]>;
    territoriosSubject: BehaviorSubject<Territorio[]>;

    selecionado: Observable<any>;
    selecionadoSubject: BehaviorSubject<any>;

    tipoImoveisFiltrados: TipoImovel[] = [];
    isLoadingTipoImovel: boolean = false;
    tipoImovelSelecionado: TipoImovel;

    resultadoPesquisaTerritorio: Territorio[] = [];
    carregandoPesquisaTerritorio: boolean = false;
    territorioSelecionada: Territorio;

    colunasTabela: string[] = ['territorio'];
    dadosTabela: Territorio[] = [];

    selectedParent: FoodNode = null;

    dado: Observable<any>;
    dadoSubject: BehaviorSubject<any>;

    item: Observable<any>;
    itemSubject: BehaviorSubject<any>;

    mostrarPopup: boolean = false;

    constructor(
        private httpImovelService: HttpImovelService,
        private apiTerritorioService: HttpTerritorioService,
        public matSnackBar: MatSnackBar,
        private apiItemListaService: HttpItemListaService
    ) {
        this.irmaosSubject = new BehaviorSubject<Imovel[]>(null);
        this.irmaos = this.irmaosSubject.asObservable();
        this.territoriosSubject = new BehaviorSubject<Territorio[]>(null);
        this.territorios = this.territoriosSubject.asObservable();
        this.selecionadoSubject = new BehaviorSubject<any[]>(null);
        this.selecionado = this.selecionadoSubject.asObservable();

        this.dadoSubject = new BehaviorSubject<any[]>(null);
        this.dado = this.dadoSubject.asObservable();

        this.itemSubject = new BehaviorSubject<any[]>(null);
        this.item = this.itemSubject.asObservable();


        this.apiTerritorioService.getHierarquia().subscribe(data => {
            console.log('dados hierarquizados: ', data);
            this.dataSource.data = this.montarHierarquia(data);
            this.treeControl.expandAll();
            
        })

    }

    ngOnInit() {


        this.configurarDropdownTerritorio();
        this.pesquisarIrmaos();
        /* if (this.formGroup.controls['territorio'].value) {
          this.pesquisarIrmaos();
          this.territoriosSubject.next([this.formGroup.controls['territorio'].value]);
          console.log('territorios: ', this.territorios);
         
        } */
    }
    
    montarHierarquia(dados: Territorio[]) {

        let arvore = [];
        dados.forEach(element => {
            arvore.push({
                elemento: element,
                children: this.montarChildren(element)
            })
        })
        return arvore;
    }

    montarChildren(elemento) {
        let arvore = [];
        if (elemento.filhos) {
            elemento.filhos.forEach(element => {
                arvore.push({
                    elemento: element,
                    children: this.montarChildren(element)
                })
            })
        }
        return arvore;
    }

    ngAfterViewInit(): void {
        console.log('mapa: ', this.mapa.map.updateSize());   
    }



    configurarDropdownTerritorio() {
        this.apiTerritorioService.pesquisarPorNome('')
            .subscribe((territorios: Territorio[]) => {
                console.log('territorios: ', territorios);

                this.territoriosSubject.next(territorios);
            });

    }

    pesquisarIrmaos() {
        this.httpImovelService.pesquisarTodos().pipe(delay(0),
            map((data: Imovel[]) => this.tratarSucessoAoPesquisarPorTerritorio(data)),
            catchError(err => this.tratarErroAoPesquisarPorTerritorio(err))).subscribe();
    }
    tratarSucessoAoPesquisarPorTerritorio(data: Imovel[]) {

        this.irmaosSubject.next(data);
        console.log('irmãos: ', this.irmaos);

    }
    tratarErroAoPesquisarPorTerritorio(erro): any {
        this.abrirSnackBar("Ops... " + erro.error, null, "red-snackbar")
        return [];
    }
    abrirSnackBar(message: string, action: string, classe: string) {
        this.matSnackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

    private _transformer = (node: FoodNode, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            elemento: node.elemento,
            level: level,
        };
    }
    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    checklistSelection = new SelectionModel<FoodNode>(true /* multiple */);
    treeFlattener = new MatTreeFlattener(
        this._transformer, node => node.level, node => node.expandable, node => node.children);

    dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    selectionModel = new SelectionModel<FoodNode>(true, []);
    
       selecionar(node) {
       
        
            //se estiver selecionando mistra o popup
        if (!this.selectionModel.isSelected(node)) {
            this.mostrarPopup = true;
            //Para nao selecionar mais de um elemento
            this.selectionModel.clear();
        } else {
            this.mostrarPopup = false;
        }
        //seleciona ou deseleciona um elemento
        this.selectionModel.toggle(node);
        console.log('O elemento que está sendo selecionado ou nao: ', node.elemento);

        if (this.selectionModel.isEmpty()) {
            this.selecionadoSubject.next(null);
        } else {
            this.selecionadoSubject.next(node.elemento);

        }

        this.dadoSubject.next(node.elemento);
        this.carregaItemDado(node.elemento);
    }

    exibirCheckbox() {

        this.selectionModel = new SelectionModel<any>(true, []);

    }

    /** Se o número de elementos selecionados corresponde ao número total de linhas. */
    todosElementosEstaoSelecionados() {

        const numeroElementosSelecionados = this.selectionModel.selected.length;
        const numeroTotalElementos = this.dataSource.data.length;
        return numeroElementosSelecionados === numeroTotalElementos;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.todosElementosEstaoSelecionados() ?
            this.selectionModel.clear() :
            this.selectionModel.clear();
    }
    verDentro(children, id) {
        children.forEach(element => {
            console.log('cada um2: ', element);
            //if (element.elemento.id === id) this.selecionarApartirDoMapa(element);
            if (element.children) this.verDentro(element.children, id);

        });
    }
    aoSelecionarNoMapa(event) {
        console.log('evento ao selecionar no mapa: ', event);
        if (!event)
            this.selectionModel.clear();
        else {
            this.dataSource.data.forEach(row => {
                console.log('cada um1: ', row.elemento.id, '/n elemento de comparação: ', event.id_);
                console.log('resultado da comparação: ', row.elemento.id == event.id_)

                if (row.elemento.id == event.id_) {
//                   this.selecionarApartirDoMapa(row);
                };
                console.log('esta selecionado: ', this.selectionModel.isSelected(row));
                console.log('selecioandos: ', this.selectionModel.selected);
                this.verDentro(row.children, event.id_);
            })
        }
    }
    /* selecionarApartirDoMapa(node) {
        //se estiver selecionando mistra o popup
    if (!this.selectionModel.isSelected(node)) {
        this.mostrarPopup = true;
        //Para nao selecionar mais de um elemento
        this.selectionModel.clear();
    } else {
        this.mostrarPopup = false;
    }
    //seleciona ou deseleciona um elemento
    this.selectionModel.toggle(node);
    node.isSelected = !node.isSelected;
    console.log('O elemento que está sendo selecionado ou nao: ', node.elemento);

    this.dadoSubject.next(node.elemento);
    this.carregaItemDado(node.elemento);
} */
    carregaItemDado(dado) {

        if (dado && dado.nome) {

            this.apiItemListaService.pesquisarPorTerritorioId(dado.id).subscribe((data: ItemLista[]) => {
                if (data.length === 0) {
                    this.itemSubject.next(null);
                } else {


                    let item = JSON.parse(JSON.stringify(data[0]));
                    item.reconhecimento.conteudo.question1 = 0;
                    item.reconhecimento.conteudo.question2 = 0;
                    item.reconhecimento.conteudo.question3 = 0;
                    item.reconhecimento.conteudo.question4 = 0;
                    item.reconhecimento.conteudo.question5 = 0;
                    data.forEach(element => {

                        item.reconhecimento.conteudo.question1 += parseInt(element.reconhecimento.conteudo.question1);
                        item.reconhecimento.conteudo.question2 += parseInt(element.reconhecimento.conteudo.question2);
                        item.reconhecimento.conteudo.question3 += parseInt(element.reconhecimento.conteudo.question3);
                        item.reconhecimento.conteudo.question4 += parseInt(element.reconhecimento.conteudo.question4);
                        item.reconhecimento.conteudo.question5 += parseInt(element.reconhecimento.conteudo.question5);
                    });




                    /* this.delayAnyWhere(item); */
                    this.itemSubject.next(item);
                    console.log('Esse é  novo item do popUp: ', item);
                }
            })
        }
        else {
            this.apiItemListaService.pesquisarPorImovelId(dado.id).subscribe((data: ItemLista) => {
                this.itemSubject.next(data);
            })
        }
    }
    fecharPopup(){
        this.mostrarPopup = false;
        this.selectionModel.clear();
    }

    teste() {
        return JSON.stringify(this.teste2);
    }

    teste2: any;
}
