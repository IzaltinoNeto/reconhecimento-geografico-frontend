import { Observable } from 'rxjs';
import { HttpItemListaService } from './../lista-de-trabalho/service/http-item-lista.service';
import { Component, OnInit, Input } from '@angular/core';
import ItemLista from '../model/itemLista';



@Component({
  selector: 'app-dado-selecionado',
  templateUrl: './dado-selecionado.component.html',
  styleUrls: ['./monitoramento.component.scss'],
})
export class DadoSelecionadoComponent implements OnInit {
  
  @Input() dado : any;
  @Input() item : ItemLista;
  constructor(
  ) {}

  ngOnInit() {
  }
  
}
