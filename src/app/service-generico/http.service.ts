import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export abstract class HttpGenerico<T> {

    public urlPadrao: string = environment.defaultUrl;

    constructor(protected http: HttpClient, protected urlApi: string) { }

    

    salvar(modelo: T): Observable<T> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}`;
        return this.http.post(requestUrl, modelo)
            .pipe(
                map((res: T) => res)
            );
    }

    pesquisarPorId(modeloId: string): Observable<T> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}/${modeloId}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: T) => res)
            );
    }

    atualizar(modelo: any): Observable<T> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}/${modelo.id}`;
        return this.http.put(requestUrl, modelo)
            .pipe(
                map((res: T) => res)
            );
    }

    atualizarSemId(modelo: any): Observable<T> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}`;
        return this.http.put(requestUrl, modelo)
            .pipe(
                map((res: T) => res)
            );
    }

    deletar(modelo: any): Observable<T> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}/${modelo.id}`;
        return this.http.delete(requestUrl)
            .pipe(
                map((res: T) => res)
            );
    }

    pesquisarPorNome(nome: string): Observable<T[]> {
        if (typeof nome !== 'string') {
            nome = "";
        }
        const href = this.urlPadrao + this.urlApi + `/byName`;
        const requestUrl =
            `${href}?nome=${nome}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: T[]) => res)
            );
    }

}
