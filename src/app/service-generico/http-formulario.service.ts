import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import {Formulario} from 'src/app/model/Formulario';
@Injectable({
    providedIn: 'root'
})
export class HttpFormularioService extends HttpGenerico<Formulario> {

    constructor(http: HttpClient) {
        super(http, '/formulario')
    }

  


}
