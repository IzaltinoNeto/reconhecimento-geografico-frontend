import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import Categoria from 'src/app/model/Categoria';
import FonteDado from '../model/fonteDado';
import TipoImovel from '../model/tipoImovel';
@Injectable({
    providedIn: 'root'
})
export class HttpTipoImovelService extends HttpGenerico<TipoImovel> {

    constructor(http: HttpClient) {
        super(http, '/tipo-imovel')
    }

  


}
