import { DadoSelecionadoComponent } from './monitoramento/dado-selecionado.component';
import { ConsolidacaoPorTerritorioListarComponent } from './consolidacao/consolidacao-por-territorio-listar.component';
import { ConsolidacaoPorListaListarComponent } from './consolidacao/consolidacao-por-lista-listar.component';
import { ListaDeTrabalhoListarComponent } from './lista-de-trabalho/lista-de-trabalho-listar.component';
import { ListaDeTrabalhoFormComponent } from './lista-de-trabalho/lista-de-trabalho-form.component';
import { ListaDeTrabalhoCriarComponent } from './lista-de-trabalho/lista-de-trabalho-criar.component';
import { TerritorioEditarComponent } from './territorio/territorio-editar.component';
import { DialogoConfirmacaoComponent } from './dialogo-confirmacao/dialogo-confirmacao.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapaDinamicoComponent } from './mapa-dinamico/mapa-dinamico.component';
import {  TerritorioFormComponent } from './territorio/territorio-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatGridListModule, MatToolbarModule, MatDialogModule, MatSnackBarModule, MatInputModule, MatAutocompleteModule, MatOptionModule, MatSpinner, MatProgressSpinnerModule, MatIconModule, MatTableModule, MatPaginatorModule, MatSortModule, MatTabsModule, MatTreeModule, MatSidenavModule, MatListModule} from '@angular/material';
import {  TerritorioCriarComponent } from './territorio/territorio-criar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {  TerritorioListarComponent } from './territorio/territorio-listar.component';
import { ImovelFormComponent } from './imovel/imovel-form.component';
import { ImovelCriarComponent } from './imovel/imovel-criar.component';
import { ImovelEditarComponent } from './imovel/imovel-editar.component';
import { ImovelListarComponent } from './imovel/imovel-listar.component';
import { ListaDeTrabalhoImoveisComponent } from './lista-de-trabalho/lista-de-trabalho-imoveis.component';
import { ListaDeTrabalhoEditarComponent } from './lista-de-trabalho/lista-de-trabalho-editar.component';
import { WebDataRocksPivot } from './webdatarocks/webdatarocks.angular4';
import { RelatorioComponent } from './consolidacao/relatorio.component';
import { MonitoramentoComponent } from './monitoramento/monitoramento.component';
import { DadosReconhecimentoComponent } from './dados-reconhecimento/dados-reconhecimento.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [
    AppComponent,
    MapaDinamicoComponent,
    TerritorioFormComponent,
    DialogoConfirmacaoComponent,
    TerritorioCriarComponent,
    TerritorioListarComponent,
    TerritorioEditarComponent,
    ImovelFormComponent,
    ImovelCriarComponent,
    ImovelEditarComponent,
    ImovelListarComponent,
    ListaDeTrabalhoCriarComponent,
    ListaDeTrabalhoFormComponent,
    ListaDeTrabalhoListarComponent,
    ListaDeTrabalhoImoveisComponent,
    ListaDeTrabalhoEditarComponent,
    ConsolidacaoPorListaListarComponent,
    ConsolidacaoPorTerritorioListarComponent,
    WebDataRocksPivot,
    RelatorioComponent,
    MonitoramentoComponent,
    DadosReconhecimentoComponent,
    DadoSelecionadoComponent
  ],
  entryComponents: [DialogoConfirmacaoComponent, DadosReconhecimentoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatGridListModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatProgressSpinnerModule,
    MatIconModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ScrollDispatchModule,
    MatIconModule,
    MatTabsModule,
    MatTreeModule,
    DragDropModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule
    
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
