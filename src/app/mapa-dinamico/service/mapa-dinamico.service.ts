import { Injectable } from '@angular/core';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { Attribution } from 'ol/control.js';
import { Circle as CircleStyle, Fill, Stroke, Style, Text } from 'ol/style.js';


@Injectable({
  providedIn: 'root'
})
export class MapaDinamicoService {

  source: any;
  sourceParent: any;
  sourceRelated: any;
  tileSource: VectorSource;
  style: Style;
  styleParent: Style;
  styleRelated: Style;
  styleSelect: Style;

  constructor() {
    //define o source de uma camada do mapa
    this.source = new VectorSource(
    );
    this.sourceParent = new VectorSource(
    );
    this.sourceRelated = new VectorSource(
    );
    //define os source da camada base de mapa(nesse caso, o googlemaps)
    this.tileSource = new OSM({
      url: 'http://mt{0-3}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
      attributions: [
        new Attribution({ html: '© Google' }),
        new Attribution({ html: '<a href="https://developers.google.com/maps/terms">Terms of Use.</a>' })
      ]
    })
    //define o estilo dos componentes de mapa
    this.style = new Style({
      fill: new Fill({
        color: 'rgba(39, 118, 245,0.2)'
      }),
      stroke: new Stroke({
        color: '#3366FF',
        width: 2,
        lineDash: [4, 5]
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#ffcc33'
        })
      }),
      text: this.getStyleText()
    })

    this.styleParent = new Style({
      fill: new Fill({
        color: 'rgba(255, 197, 115,0.05)'
      }),
      stroke: new Stroke({
        color: '#ffc573',
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#ffcc33'
        })
      }),
      text: this.getStyleText()
    });

    this.styleRelated = new Style({
      fill: new Fill({
        color: 'rgba(156, 194, 255,0.4)'
      }),
      stroke: new Stroke({
        color: '#9cc2ff',
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#ffcc33'
        })
      }),
      text: this.getStyleText()
    });
    this.styleSelect = new Style({
      fill: new Fill({
        color: 'rgba(39, 118, 245,0.4)'
      }),
      stroke: new Stroke({
        color: '#2776f5',
        width: 3,
        lineDash: [4, 5]
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#16d930'
        })
      }),
      text: this.getStyleText()
    })

  }

  getTileSource(): VectorSource {
    return this.tileSource;
  }
  getSource(): VectorSource {
    return this.source;
  }
  getSourceParent(): VectorSource {
    return this.sourceParent;
  }
  getSourceRelated(): VectorSource {
    return this.sourceRelated;
  }

  getStyle(): Style {
    return this.style;
  }

  getStyleParent(): Style {
    return this.styleParent;
  }
  getStyleRelated(): Style {
    return this.styleRelated;
  }
  getStyleSelect(): Style {
    return this.styleSelect;
  }
  getNewStyleRelated():Style{
    return new Style({
      fill: new Fill({
        color: 'rgba(156, 194, 255,0.4)'
      }),
      stroke: new Stroke({
        color: '#9cc2ff',
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#ffcc33'
        })
      }),
      text: this.getStyleText()
    });
  }
  getNewStyleParent():Style{
    return new Style({
      fill: new Fill({
        color: 'rgba(255, 197, 115,0.05)'
      }),
      stroke: new Stroke({
        color: '#ffc573',
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: '#ffcc33'
        })
      }),
      text: this.getStyleText()
    });
  }

  getStyleText():Text {
    return new Text({
      font: '10px sans-serif',
      fill: new Fill({ color: '#bdbdbd' }),
      
      text : ''
    });
  }
  
}


