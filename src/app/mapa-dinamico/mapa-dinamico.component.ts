import { Style } from 'ol/style.js';
import { MapaDinamicoService } from './service/mapa-dinamico.service';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import View from 'ol/View.js';
import Map from 'ol/Map.js';
import { Draw, Modify, Snap } from 'ol/interaction.js';
import Select from 'ol/interaction/Select.js';
import { click, pointerMove, altKeyOnly } from 'ol/events/condition.js';
import Polygon from 'ol/geom/Polygon';
import Feature from 'ol/Feature';
import { getCenter } from 'ol/extent';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { Observable, Subject } from 'rxjs';
import Territorio from '../model/territorio';
import {defaults as defaultControls, OverviewMap} from 'ol/control.js';

@Component({
  selector: 'app-mapa-dinamico',
  templateUrl: './mapa-dinamico.component.html',
  styleUrls: ['./mapa-dinamico.component.scss']
})
export class MapaDinamicoComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild("mapElement") mapElement: ElementRef;
  public map: any;
  public draw: any;
  public snap: any;
  public source: any;
  public selectClick: any;
  @Output() polygonOut = new EventEmitter();
  @Output() selected = new EventEmitter();
  @Input() polygonIn: string;
  @Input() NamePolygonIn: string;
  @Input() polygonParentIn: Observable<any[]>;
  @Input() polygonRelatedIn: Observable<any[]>;
  @Input() selecionado : Observable<any>;
  constructor(public mapaDinamicoService: MapaDinamicoService) {
    this.selectClick = new Select({
      condition: click,
      style: this.mapaDinamicoService.getStyleSelect()
    });
    this.apagarFeatures();
  }




  ngOnInit() {
    this.renderizarComponente();
    this.adicionarInteracaoModificarPoligono();
    console.log('poligono relacionado', this.polygonRelatedIn);
    //Adiciona os poligonos relacionados/irmãos ao atual
    this.polygonRelatedIn.subscribe(x => {
      console.log('poligonos Related: ', x);

      if (x) {
        this.mapaDinamicoService.getSourceRelated().clear();
        x.forEach(element => {
          let polRelated = new Polygon([element.poligono]);
          let polRelatedFeature = new Feature(polRelated);
          polRelatedFeature.setId(element.id);

          console.log('poligono Relacionado para alterar cor: ', polRelatedFeature);
          polRelatedFeature.setStyle(this.mapaDinamicoService.getNewStyleRelated());

          polRelatedFeature.getStyle().getText().text_ = element.nome ? element.nome : element.numero;
          this.mapaDinamicoService.getSourceRelated().
            addFeature(polRelatedFeature);
        });
      }
    }
    )
    //Adiciona o poligono Pai/Superior ao atual
    this.polygonParentIn.subscribe(x => {
      if (x) {
        this.mapaDinamicoService.getSourceParent().clear();

        x.forEach(element => {
          let polF = new Polygon([element.poligono]);

          let polFeature = new Feature(polF);
          polFeature.set('name', element.nome);
          polFeature.setId(element.id);
          console.log('texto do poligno');
          polFeature.setStyle(this.mapaDinamicoService.getNewStyleParent());

          polFeature.getStyle().getText().text_ = element.nome;

          this.mapaDinamicoService.getSourceParent().
            addFeature(polFeature);

          if (!this.polygonIn) {
            this.map.setView(new View({
              projection: 'EPSG:4326',
              center: getCenter(polF.getExtent()),
              zoom: 18
            }));
          }
        });

      }

    })
    //Adiciona o poligono atual
    if (this.polygonIn) {
      let pol = new Polygon([JSON.parse(this.polygonIn)]);
      console.log('coordenadas do poligono: ', [JSON.parse(this.polygonIn)]);
      let polFeature = new Feature(pol);
      this.mapaDinamicoService.getSource().addFeature(
        polFeature);
      this.mapaDinamicoService.getStyle().getText().text_ = this.NamePolygonIn;
      
      this.map.setView(new View({
        projection: 'EPSG:4326',
        center: JSON.parse(this.polygonIn)[0],
        zoom: 19
      }));
      this.selectClick.getFeatures().push(polFeature);
      this.mapaDinamicoService.getStyleSelect().getText().text_ = this.NamePolygonIn;
      this.delaySelectActivate(this.map, this.selectClick);

    }

    if(this.selecionado){
        this.selecionado.subscribe(x=>{
            if(x){
      
            
            console.log('selecionado no componente mapa: ',x);
            console.log('sourceparent',this.mapaDinamicoService.getSourceParent().getFeatures());
            let resultado;
            
            if(x.nome){
              resultado = this.mapaDinamicoService.getSourceParent().getFeatures().filter(data=>{
                return x.id === data.id_;
              });
            }
            else{
              resultado = this.mapaDinamicoService.getSourceRelated().getFeatures().filter(data=>{
                return x.id === data.id_;
              });
            } 
            /* let select = new Select({
              condition: click
            }); */
            /*  let pol = new Polygon([JSON.parse(this.polygonIn)]);
            console.log('coordenadas do poligono: ', [JSON.parse(this.polygonIn)]); */
            let polFeature = new Feature(resultado[0].getGeometry());
            console.log('resultado fo filtro: ', resultado[0].getGeometry());
           /*  this.mapaDinamicoService.getSource().addFeature(
              polFeature); */
              this.selectClick.getFeatures().clear();
            this.selectClick.getFeatures().push(polFeature);
            this.map.setView(new View({
              projection: 'EPSG:4326',
              center: getCenter(resultado[0].getGeometry().getExtent()),
              zoom: 16
            }));
            //this.delaySelectActivate(this.map, this.selectClick);
            console.log('selecionados do selectclick', this.selectClick.getFeatures());
                  
          }
          else {
            this.selectClick.getFeatures().clear();
          } 
          })
        
    }
    
  }
  ngAfterViewInit(): void {
    this.map.setTarget(this.mapElement.nativeElement.id);
    this.delaySelectActivate(this.map, this.selectClick);
    //this.adicionarInteracaoDesenharPoligono();
  }

  renderizarComponente() {
    this.map = new Map({
      controls: defaultControls().extend([
        new OverviewMap()
      ]),
      target: 'map',
      layers: [
        new TileLayer({
          source: this.mapaDinamicoService.getTileSource()
        })
        ,
        new VectorLayer({
          source: this.mapaDinamicoService.getSourceParent(),
          style: this.mapaDinamicoService.getStyleParent()
        }),
        new VectorLayer({
          source: this.mapaDinamicoService.getSourceRelated(),
          style: this.mapaDinamicoService.getStyleRelated()
        }),
        new VectorLayer({
          source: this.mapaDinamicoService.getSource(),
          style: this.mapaDinamicoService.getStyle()
        }),
      ],
      view: new View({
        projection: 'EPSG:4326',
        center: [-59.994984540209266, -3.028787880138466],
        zoom: 14
      })
    });
  }

  adicionarInteracaoDesenharPoligono() {
    this.draw = new Draw({
      source: this.mapaDinamicoService.getSource(),
      type: 'Polygon',
      /* style: this.mapaDinamicoService.getStyleSelect() */
    });

    this.map.addInteraction(this.draw);
    this.snap = new Snap({ source: this.mapaDinamicoService.getSource() });
    this.map.addInteraction(this.snap);
    this.draw.on('drawstart', function (event) {

      //selectClick.setActive(false);
      //selectedFeatures.clear(); 
    }, this);
    let selectClick = this.selectClick;
    let polygon = this.polygonOut;
    let map = this.map;
    let draw = this.draw;
    let snap = this.snap;
    this.draw.on('drawend', (event) => {
console.log('event: ',event)
      //seleciona o poligono criado recentemente
      if (selectClick.getFeatures())
        selectClick.getFeatures().push(event.feature);

      this.delaySelectActivate(map, selectClick);

      polygon.emit(event.feature.getGeometry());
      //console.log('poligono', polygon);
      //remove as interações
      map.removeInteraction(draw);
      map.removeInteraction(snap);

    });
  }
  adicionarInteracaoModificarPoligono() {
    let modify = new Modify({ source: this.mapaDinamicoService.getSource() });
    this.map.addInteraction(modify);
    let polygon = this.polygonOut;
    modify.on('modifyend', function (event) {
      console.log('selecionados: Agora', event.features.array_[0].getGeometry());

      polygon.emit(event.features.array_[0].getGeometry());
    });
  }

  delaySelectActivate(map, selectClick) {
    
    if(this.selected) {
      var selected = this.selected;
    }
    
    setTimeout( ()=> {
      //selectClick.setActive(true);
      
      map.addInteraction(selectClick);
      selectClick.on('select', (e)=> {
        if(e.selected[0])
        {let fet = new Feature(e.selected[0].getGeometry());
          selectClick.getFeatures().clear();
        selectClick.getFeatures().push(fet);
      }
        console.log(e.target.getFeatures());
        console.log('selecionado: >>>>', e.selected[0]);
        if(this.selected)
          this.selected.emit(e.selected[0]);


      });
      /*  var mover = new Drag();
       map.addInteraction(mover); */
    }, 300);
  }

  apagarFeatures() {
    this.mapaDinamicoService.getSource().clear();
    this.mapaDinamicoService.getSourceRelated().clear();
    this.mapaDinamicoService.getSourceParent().clear();
    this.selectClick.getFeatures().clear();
  }

  ngOnDestroy() {
    this.apagarFeatures();

  }
}
