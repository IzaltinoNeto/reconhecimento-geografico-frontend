import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaDinamicoComponent } from './mapa-dinamico.component';

describe('MapaDinamicoComponent', () => {
  let component: MapaDinamicoComponent;
  let fixture: ComponentFixture<MapaDinamicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaDinamicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaDinamicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
