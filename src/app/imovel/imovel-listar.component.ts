import { HttpImovelService } from './service/http-imovel.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, tap, finalize } from 'rxjs/operators';
import Imovel from '../model/imovel';
import { MatPaginator, MatSort } from '@angular/material';
import { Page } from '../model/Page';
import Territorio from '../model/territorio';
import { HttpTerritorioService } from '../territorio/service/http-territorio.service';

@Component({
    selector: 'app-imovel-listar',
    templateUrl: './imovel-listar.component.html',
    styleUrls: ['./imovel.component.scss']
})
export class ImovelListarComponent implements OnInit {

    //Atributos para selecao de filtro por imovel Pai
    resultadoPesquisaTerritorio: Territorio[] = [];
    carregandoPesquisaTerritorio: boolean = false;
    territorioSelecionada: Territorio;
    formGroup: FormGroup = this.formBuilder.group({
        "territorio": new FormControl("")
    });

    //Atributos para tabela
    colunasTabela: string[] = ['logradouro', 'numero', 'tipoImovel', 'territorio'];
    dadosTabela: Imovel[] = [];

    quantidadeRegistros = 0;
    carregandoResultados = true;
    tempoLimiteAtingido = false;

    filtro: string = "";
    tamanhoMinimoTextoFiltro: number = 2;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private apiTerritorioService: HttpTerritorioService,
        private apiImovelService: HttpImovelService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.configurarDropdown();
        this.configurarTabela();
    }

    configurarTabela() {
        // Se o imovel muda a ordem na tabela, volta para primeira pagina
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.carregandoResultados = true;
                        return this.apiImovelService.pesquisarComFiltro(
                            this.territorioSelecionada,
                            this.filtro,
                            this.sort.active,
                            this.sort.direction,
                            this.paginator.pageIndex,
                            this.paginator.pageSize | 30
                        );
                    
                }),
                map((resposta: Page<Imovel>) => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = false;
                    this.quantidadeRegistros = resposta.totalElements;
                    return resposta.content;
                }),
                catchError(() => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = true;
                    return observableOf([]);
                })
            ).subscribe(resposta =>{ 
                console.log('dados da tabela: ', resposta);
                return this.dadosTabela = resposta;});
    }

    aplicarFiltroTabela() {
        if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
            this.paginator.page.emit();
        } else {
            this.filtro = "";
        }

        this.filtrarImoveis();
    }

    limparFiltroTabela() {
        this.filtro = "";
        this.paginator.page.emit();
        this.limparFiltroTerritorioSelecionada();
    }

    aoDigitarFiltroTabela(filtro: string) {
        this.filtro = filtro;
        if (this.filtro.length === 0) {
            this.paginator.page.emit();
        }
    }

    configurarDropdown() {
        this.formGroup
            .get('territorio')
            .valueChanges
            .pipe(
                debounceTime(300),
                tap(() => this.carregandoPesquisaTerritorio = true),
                switchMap(textoPesquisado => this.apiTerritorioService.pesquisarPorNome(textoPesquisado)
                    .pipe(
                        finalize(() => this.carregandoPesquisaTerritorio = false),
                    )
                )
            )
            .subscribe(resultadoPesquisa => {
                this.resultadoPesquisaTerritorio = resultadoPesquisa;
            });

        this.formGroup.get("territorio").setValue(this.formGroup.value.territorio);
    }

    selecionarTerritorio(event: any) {
        this.territorioSelecionada = event.option.value;
    }

    checarSelecaoTerritorio() {
        if (!this.territorioSelecionada || this.territorioSelecionada !== this.formGroup.controls['territorio'].value) {
            this.formGroup.controls['territorio'].setValue(null);
            this.territorioSelecionada = null;
        }
    }

    formatarExibicaoTerritorio(territorio: Territorio) {
        if (territorio) { return territorio.nome; }
    }

    filtrarImoveis() {
        console.log("Territorio", this.territorioSelecionada);
        this.paginator.page.emit();
    }

    limparFiltroTerritorioSelecionada() {
        this.formGroup.controls['territorio'].setValue(null);
        this.territorioSelecionada = null;
        this.paginator.page.emit();
    }

}
