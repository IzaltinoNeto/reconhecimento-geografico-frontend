import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import { FormGroup} from '@angular/forms';
import Imovel from '../model/imovel';
import { ImovelFormService } from './service/imovel-form.service';
import { HttpImovelService } from './service/http-imovel.service';

/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-imovel-criar',
    templateUrl: './imovel-criar.component.html',
    styleUrls: ['./imovel.component.scss']
})

export class ImovelCriarComponent implements OnInit {

    formGroup: FormGroup;
    
    imovel: Imovel;

    constructor(
        private apiDataService: HttpImovelService,
        private router: Router,
        public snackBar: MatSnackBar,
        private dialog: MatDialog,     
        private imovelFormService: ImovelFormService
        ) {
    }

    ngOnInit() {
        this.formGroup = this.imovelFormService.getNewFormGroup();
        this.imovel = new Imovel();  
    }

    salvar() {
        console.log("Salvar Imovel!", this.formGroup.value);

        const dialogoConfirmacao = this.dialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Salvar", conteudo: "Confirma criação?" }
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                Imovel.popularBaseadoEmFormGroup(this.imovel, this.formGroup);
                this.apiDataService.salvar(this.imovel).pipe(
                    map((data: Imovel) => this.onSuccess(data)),
                    catchError(err => this.handleError(err))).subscribe();
            }
        });
    }

    onSuccess(data) {
        console.log("Tudo OK!", data)
        this.abrirSnackBar("Imovel cadastrado com sucesso!", null, "green-snackbar")
        this.router.navigate([`imoveis`]);  
    }

    handleError(erro): any {
        console.log("Erro", erro)
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
