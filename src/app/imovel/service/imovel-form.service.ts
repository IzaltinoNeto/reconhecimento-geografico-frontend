import { Injectable } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';


@Injectable({
    providedIn: 'root'
})
export class ImovelFormService {

    constructor(
        private formBuilder: FormBuilder
    ) {
    }
    getNewFormGroup() {
        let id = new FormControl("");
        let tipoImovel = new FormControl("", Validators.required);
        let pontoEstrategico = new FormControl("");
        let territorio = new FormControl("", Validators.required);
        let logradouro = new FormControl("", Validators.required);
        let cep = new FormControl("", Validators.pattern(/^\d{8}$/));
        let numero = new FormControl("", Validators.required);
        let complemento = new FormControl("");
        let sequencia = new FormControl("");
        let numQuarteirao = new FormControl("");
        let ladoQuarteirao = new FormControl("");
        let poligono = new FormControl("");
        

        let formGroup = this.formBuilder.group({
            "id": id,
            "logradouro": logradouro,
            "tipoImovel": tipoImovel,
            "pontoEstrategico": pontoEstrategico,
            "territorio": territorio,
            "cep": cep,
            "numero": numero,
            "complemento": complemento,
            "sequencia": sequencia,
            "numQuarteirao": numQuarteirao,
            "ladoQuarteirao": ladoQuarteirao,
            "poligono": poligono
        });
        return formGroup;
    }

}
