import { Page } from '../../model/Page';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import Imovel from 'src/app/model/imovel';
import Territorio from 'src/app/model/territorio';
@Injectable({
    providedIn: 'root'
})
export class HttpImovelService extends HttpGenerico<Imovel> {

    constructor(http: HttpClient) {
        super(http, '/imovel')
    }

    pesquisarComFiltro(territorio: Territorio,nome:string, sort: string, order: string, page: number, size: number): Observable<Page<Imovel>> {
        if (typeof nome !== 'string') {
            nome = "";
        }

        const href = this.urlPadrao + this.urlApi + '/byLogradouroWithPagination';
        const requestUrl =
            `${href}?nome=${nome}&page=${page}&size=${size}`+ (territorio ? `&territorioId=${territorio.id}` : "");
        return this.http.get(requestUrl)
            .pipe(
                map((res: Page<Imovel>) => res)
            );
    }
   
    pesquisarPorTerritorio(id : string, territorio: Territorio): Observable<Imovel[]> {
        if(!id)
            id = '0';
        const href = this.urlPadrao + this.urlApi + `/byTerritorio`;
        const requestUrl =
            `${href}?territorioId=${territorio.id}&imovelId=${id}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Imovel[]) => res)
            );
    }

    pesquisarTodos(): Observable<Imovel[]> {
        
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Imovel[]) => res)
            );
    }

}
