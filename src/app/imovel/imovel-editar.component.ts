import { HttpImovelService } from './service/http-imovel.service';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import Imovel from '../model/imovel';
import { FormGroup } from '@angular/forms';
import { ImovelFormService } from './service/imovel-form.service';
import { delay } from 'rxjs/operators';
/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-imovel-editar',
    templateUrl: './imovel-editar.component.html',
    styleUrls: ['./imovel.component.scss']
})

export class ImovelEditarComponent implements OnInit {

    formGroup: FormGroup;
    
    imovel: Imovel = new Imovel();
    pronto : boolean = false;
    id: string;
    imovelsIrmaos : Imovel[];

    constructor(
        private apiImovelService: HttpImovelService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        public matSnackBar: MatSnackBar,
        private matDialog: MatDialog,
        private imovelFormService: ImovelFormService
    ) {
        this.formGroup = this.imovelFormService.getNewFormGroup();
        this.activatedRoute.params.subscribe(params => this.id = params.id);
        
    }

    ngOnInit() {
        this.pesquisarPorId();
    }
    pesquisarPorId() {
        this.apiImovelService.pesquisarPorId(this.id).pipe( delay(0),
            map((data: Imovel) => this.tratarSucessoAoPesquisarPorId(data)),
            catchError(err => this.tratarErroAoPesquisarPorId(err))).subscribe();
    }
    atualizar() {
        const dialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: {titulo: "Alterar", conteudo: "Confirma alterações?"}
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                Imovel.popularBaseadoEmFormGroup(this.imovel, this.formGroup);
                console.log('Atualizar-> ',this.imovel);
                this.apiImovelService.atualizarSemId(this.imovel).pipe(
                    map((data: Imovel) => this.tratarSucessoAoAtualizar(data)),
                    catchError(err => this.tratarErroAoAtualizar(err))).subscribe();
            }
        });
        
    }

    tratarSucessoAoPesquisarPorId(data: Imovel) {
        this.imovel = data;
        this.formGroup.patchValue(this.imovel);
        this.formGroup.controls['poligono'].setValue(JSON.stringify(this.imovel.poligono));
        //this.pesquisarIrmaos();
        //this.pronto = true;


    }
    tratarErroAoPesquisarPorId(erro): any {
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    tratarSucessoAoAtualizar(data: Imovel) {
        this.abrirSnackBar("Registro atualizado com sucesso!",null, "green-snackbar")        
        this.router.navigate([`imoveis`]); 
    }

    tratarErroAoAtualizar(erro): any {
        console.log(erro.error);
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }
    
    

    excluir() {
        const matDialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Confirmação", conteudo: "Confirma exclusão do registro?" }
        });

        matDialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                this.apiImovelService.deletar(this.imovel).pipe(
                    map((data: Imovel) => this.tratarSucessoAoDeletar(data)),
                    catchError(err => this.tratarErroAoDeletar(err))).subscribe();
            }
        });

    }

    tratarSucessoAoDeletar(data) {
        this.abrirSnackBar("Registro deletado com sucesso!", null, "green-snackbar")
        this.router.navigate([`imovels`]);
    }

    tratarErroAoDeletar(erro): any {
        let mensagem = "";
        if(erro.message != null && erro.message.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro é pai de outros"
        } else {
            mensagem = erro;
        }

        if(erro != null && erro.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro possui relacionamento com outras entidades."
        }

        this.abrirSnackBar("Erro ao deletar: " + mensagem, null, "red-snackbar")
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.matSnackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
