import { HttpTerritorioService } from './service/http-territorio.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, tap, finalize } from 'rxjs/operators';
import Territorio from '../model/territorio';
import { MatPaginator, MatSort } from '@angular/material';
import { Page } from '../model/Page';

@Component({
    selector: 'app-territorio-listar',
    templateUrl: './territorio-listar.component.html',
    styleUrls: ['./territorio.component.scss']
})
export class TerritorioListarComponent implements OnInit {

    //Atributos para selecao de filtro por territorio Pai
    resultadoPesquisaTerritorioPai: Territorio[] = [];
    carregandoPesquisaTerritorioPai: boolean = false;
    territorioPaiSelecionada: Territorio;
    formGroup: FormGroup = this.formBuilder.group({
        "territorioPai": new FormControl("")
    });

    //Atributos para tabela
    colunasTabela: string[] = ['nome', 'categoria', 'territorioPai'];
    dadosTabela: Territorio[] = [];

    quantidadeRegistros = 0;
    carregandoResultados = true;
    tempoLimiteAtingido = false;

    filtro: string = "";
    tamanhoMinimoTextoFiltro: number = 2;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private apiTerritorioService: HttpTerritorioService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.configurarDropdown();
        this.configurarTabela();
    }

    configurarTabela() {
        // Se o territorio muda a ordem na tabela, volta para primeira pagina
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.carregandoResultados = true;
                        return this.apiTerritorioService.pesquisarComFiltro(
                            this.territorioPaiSelecionada,
                            this.filtro,
                            this.sort.active,
                            this.sort.direction,
                            this.paginator.pageIndex,
                            this.paginator.pageSize | 5
                        );
                    
                }),
                map((resposta: Page<Territorio>) => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = false;
                    this.quantidadeRegistros = resposta.totalElements;
                    return resposta.content;
                }),
                catchError(() => {
                    this.carregandoResultados = false;
                    this.tempoLimiteAtingido = true;
                    return observableOf([]);
                })
            ).subscribe(resposta =>{ 
                console.log('dados da tabela: ', resposta);
                return this.dadosTabela = resposta;});
    }

    aplicarFiltroTabela() {
        if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
            this.paginator.page.emit();
        } else {
            this.filtro = "";
        }

        this.filtrarTerritorios();
    }

    limparFiltroTabela() {
        this.filtro = "";
        this.paginator.page.emit();
        this.limparFiltroTerritorioPaiSelecionada();
    }

    aoDigitarFiltroTabela(filtro: string) {
        this.filtro = filtro;
        if (this.filtro.length === 0) {
            this.paginator.page.emit();
        }
    }

    configurarDropdown() {
        this.formGroup
            .get('territorioPai')
            .valueChanges
            .pipe(
                debounceTime(300),
                tap(() => this.carregandoPesquisaTerritorioPai = true),
                switchMap(textoPesquisado => this.apiTerritorioService.pesquisarPorNome(textoPesquisado)
                    .pipe(
                        finalize(() => this.carregandoPesquisaTerritorioPai = false),
                    )
                )
            )
            .subscribe(resultadoPesquisa => {
                this.resultadoPesquisaTerritorioPai = resultadoPesquisa;
            });

        this.formGroup.get("territorioPai").setValue(this.formGroup.value.territorioPai);
    }

    selecionarTerritorioPai(event: any) {
        this.territorioPaiSelecionada = event.option.value;
    }

    checarSelecaoTerritorioPai() {
        if (!this.territorioPaiSelecionada || this.territorioPaiSelecionada !== this.formGroup.controls['territorioPai'].value) {
            this.formGroup.controls['territorioPai'].setValue(null);
            this.territorioPaiSelecionada = null;
        }
    }

    formatarExibicaoTerritorioPai(territorioPai: Territorio) {
        if (territorioPai) { return territorioPai.nome; }
    }

    filtrarTerritorios() {
        console.log("TerritorioPai", this.territorioPaiSelecionada);
        this.paginator.page.emit();
    }

    limparFiltroTerritorioPaiSelecionada() {
        this.formGroup.controls['territorioPai'].setValue(null);
        this.territorioPaiSelecionada = null;
        this.paginator.page.emit();
    }

}
