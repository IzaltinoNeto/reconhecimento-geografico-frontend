import { HttpTerritorioService } from './service/http-territorio.service';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import Territorio from '../model/territorio';
import { FormGroup } from '@angular/forms';
import { TerritorioFormService } from './service/territorio-form.service';
import { delay } from 'rxjs/operators';
/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-territorio-editar',
    templateUrl: './territorio-editar.component.html',
    styleUrls: ['./territorio.component.scss']
})

export class TerritorioEditarComponent implements OnInit {

    formGroup: FormGroup;
    
    territorio: Territorio = new Territorio();
    pronto : boolean = false;
    id: string;
    territoriosIrmaos : Territorio[];

    constructor(
        private apiTerritorioService: HttpTerritorioService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        public matSnackBar: MatSnackBar,
        private matDialog: MatDialog,
        private territorioFormService: TerritorioFormService
    ) {
        this.formGroup = this.territorioFormService.getNewFormGroup();
        this.activatedRoute.params.subscribe(params => this.id = params.id);
        
    }

    ngOnInit() {
        this.pesquisarPorId();
    }
    pesquisarPorId() {
        this.apiTerritorioService.pesquisarPorId(this.id).pipe( delay(0),
            map((data: Territorio) => this.tratarSucessoAoPesquisarPorId(data)),
            catchError(err => this.tratarErroAoPesquisarPorId(err))).subscribe();
    }
    atualizar() {
        const dialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: {titulo: "Alterar", conteudo: "Confirma alterações?"}
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                Territorio.popularBaseadoEmFormGroup(this.territorio, this.formGroup);
                console.log('Atualizar-> ',this.territorio);
                this.apiTerritorioService.atualizarSemId(this.territorio).pipe(
                    map((data: Territorio) => this.tratarSucessoAoAtualizar(data)),
                    catchError(err => this.tratarErroAoAtualizar(err))).subscribe();
            }
        });
        
    }

    tratarSucessoAoPesquisarPorId(data: Territorio) {
        this.territorio = data;
        this.formGroup.patchValue(this.territorio);
        this.formGroup.controls['poligono'].setValue(JSON.stringify(this.territorio.poligono));
        //this.pesquisarIrmaos();
        //this.pronto = true;


    }
    tratarErroAoPesquisarPorId(erro): any {
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    tratarSucessoAoAtualizar(data: Territorio) {
        this.abrirSnackBar("Registro atualizado com sucesso!",null, "green-snackbar")        
        this.router.navigate([`territorios`]); 
    }

    tratarErroAoAtualizar(erro): any {
        console.log(erro.error);
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }
    
    

    excluir() {
        const matDialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Confirmação", conteudo: "Confirma exclusão do registro?" }
        });

        matDialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                this.apiTerritorioService.deletar(this.territorio).pipe(
                    map((data: Territorio) => this.tratarSucessoAoDeletar(data)),
                    catchError(err => this.tratarErroAoDeletar(err))).subscribe();
            }
        });

    }

    tratarSucessoAoDeletar(data) {
        this.abrirSnackBar("Registro deletado com sucesso!", null, "green-snackbar")
        this.router.navigate([`territorios`]);
    }

    tratarErroAoDeletar(erro): any {
        let mensagem = "";
        if(erro.message != null && erro.message.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro é pai de outros"
        } else {
            mensagem = erro;
        }

        if(erro != null && erro.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro possui relacionamento com outras entidades."
        }

        this.abrirSnackBar("Erro ao deletar: " + mensagem, null, "red-snackbar")
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.matSnackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
