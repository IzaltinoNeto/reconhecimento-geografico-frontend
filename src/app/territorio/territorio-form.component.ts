
import { HttpFonteDadoService } from './../service-generico/http-fonte-dado.service';
import { HttpTerritorioService } from './service/http-territorio.service';
import { HttpCategoriaService } from '../service-generico/http-categoria.service';
import { TerritorioFormService } from './service/territorio-form.service';
import { Map } from 'ol/Map.js';
import { Component, ChangeDetectionStrategy, OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize, delay, map, catchError } from 'rxjs/operators';
import Categoria from '../model/categoria';
import Territorio from '../model/territorio';
import { MatSnackBar } from '@angular/material';
import FonteDado from '../model/fonteDado';
import { Observable, BehaviorSubject } from 'rxjs';
import { MapaDinamicoComponent } from '../mapa-dinamico/mapa-dinamico.component';
@Component({
  selector: 'app-territorio-form',
  templateUrl: './territorio-form.component.html',
  styleUrls: ['./territorio.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TerritorioFormComponent implements OnInit {
  @ViewChild("mapa") mapa:MapaDinamicoComponent;
  @Input() formGroup: FormGroup;
  @Input() criando: boolean;
  @Output() excluir = new EventEmitter();
  territoriosIrmaos: Observable<Territorio[]>;
  territoriosIrmaosSubject: BehaviorSubject<Territorio[]>;

  territorioPai: Observable<Territorio[]>;
  territorioPaiSubject: BehaviorSubject<Territorio[]>;

  categoriasFiltrados: Categoria[] = [];
  isLoadingCategoria: boolean = false;
  categoriaSelecionado: Categoria;

  fonteDadosFiltrados: FonteDado[] = [];
  isLoadingFonteDado: boolean = false;
  fonteDadoSelecionado: FonteDado;

  resultadoPesquisaTerritorioPai: Territorio[] = [];
  carregandoPesquisaTerritorioPai: boolean = false;
  territorioPaiSelecionada: Territorio;


  constructor(private territorioFormService: TerritorioFormService,
    private httpCategoriaService: HttpCategoriaService,
    private httpFonteDadoService: HttpFonteDadoService,
    private apiTerritorioService: HttpTerritorioService,
    public matSnackBar: MatSnackBar,
  ) {
    this.territoriosIrmaosSubject = new BehaviorSubject<Territorio[]>(null);
    this.territoriosIrmaos = this.territoriosIrmaosSubject.asObservable();
    this.territorioPaiSubject = new BehaviorSubject<Territorio[]>(null);
    this.territorioPai = this.territorioPaiSubject.asObservable();
  }

  ngOnInit() {
    this.formGroup
      .get('categoria')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.isLoadingCategoria = true),
        switchMap(value => this.httpCategoriaService.pesquisarPorNome(value)
          .pipe(
            finalize(() => this.isLoadingCategoria = false),
          )
        )
      )
      .subscribe(categorias => {
        this.categoriasFiltrados = categorias;
      });

    this.formGroup.get("categoria").setValue(this.formGroup.value.categoria);

    this.configurarDropdownTerritorioPai();
    this.configurarDropdownFonteDado();

    if (this.formGroup.controls['territorioPai'].value) {
      this.pesquisarIrmaos( this.formGroup.controls['id'].value,
       this.formGroup.controls['territorioPai'].value);
      this.territorioPaiSubject.next([this.formGroup.controls['territorioPai'].value]);
     
    }
  }

  ngAfterViewInit(): void {
    console.log('mapa: ', this.mapa.map.updateSize());
  }
  criarPoligono() {
    this.mapa.adicionarInteracaoDesenharPoligono();
  }

  tratarPoligonoCriado(event: any) {
    console.log(event.getCoordinates());
    let coords = event.getCoordinates();
    let coordsString = JSON.stringify(coords[0]);

    console.log('poligono criado', coordsString);
    this.formGroup.controls['poligono'].setValue(coordsString);

  }


  selecionarCategoria(event: any) {
    this.categoriaSelecionado = event.option.value;
  }

  checarSelecaoCategoria() {
    if (!this.categoriaSelecionado || this.categoriaSelecionado !== this.formGroup.controls['categoria'].value) {
      this.formGroup.controls['categoria'].setValue(null);
      this.categoriaSelecionado = null;
    }
  }

  displayFnCategoria(categoria: Categoria) {
    if (categoria) { return categoria.nome; }
  }
  configurarDropdownFonteDado() {
    this.formGroup
      .get('fonteDado')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.isLoadingFonteDado = true),
        switchMap(value => this.httpFonteDadoService.pesquisarPorNome(value)
          .pipe(
            finalize(() => this.isLoadingFonteDado = false),
          )
        )
      )
      .subscribe(categorias => {
        this.fonteDadosFiltrados = categorias;
      });

    this.formGroup.get("fonteDado").setValue(this.formGroup.value.fonteDado);
  }

  selecionarFonteDado(event: any) {
    this.fonteDadoSelecionado = event.option.value;
  }

  checarSelecaoFonteDado() {
    if (!this.fonteDadoSelecionado || this.fonteDadoSelecionado !== this.formGroup.controls['fonteDado'].value) {
      this.formGroup.controls['fonteDado'].setValue(null);
      this.fonteDadoSelecionado = null;
    }
  }

  displayFnFonteDado(fonteDado: FonteDado) {
    if (fonteDado) { return fonteDado.descricao; }
  }

  configurarDropdownTerritorioPai() {
    this.formGroup
      .get('territorioPai')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.carregandoPesquisaTerritorioPai = true),
        switchMap(value => this.apiTerritorioService.pesquisarPorNome(value)
          .pipe(
            finalize(() => this.carregandoPesquisaTerritorioPai = false),
          )
        )
      )
      .subscribe(territorios => {
        this.resultadoPesquisaTerritorioPai = territorios;
      });

    this.formGroup.get("territorioPai").setValue(this.formGroup.value.territorioPai);

  }
  selecionarTerritorioPai(event: any) {
    this.territorioPaiSelecionada = event.option.value;
    this.pesquisarIrmaos(this.formGroup.controls['id'].value, this.territorioPaiSelecionada);
    this.territorioPaiSubject.next([this.territorioPaiSelecionada]);

  }

  checarSelecaoTerritorioPai() {
    if (!this.territorioPaiSelecionada || this.territorioPaiSelecionada !== this.formGroup.controls['territorioPai'].value) {
      this.formGroup.controls['territorioPai'].setValue(null);
      this.territorioPaiSelecionada = null;
    }
  }

  formatarExibicaoTerritorioPai(territorioPai: Territorio) {
    if (territorioPai) { return territorioPai.nome; }
  }
  pesquisarIrmaos( id : string,territorioPai: Territorio) {
    this.apiTerritorioService.pesquisarPorTerritorioPai(id,territorioPai).pipe(delay(0),
      map((data: Territorio[]) => this.tratarSucessoAoPesquisarPorTerritorioPai(data)),
      catchError(err => this.tratarErroAoPesquisarPorTerritorioPai(err))).subscribe();
  }
  tratarSucessoAoPesquisarPorTerritorioPai(data: Territorio[]) {

    this.territoriosIrmaosSubject.next(data);
    console.log('irmãos: ', this.territoriosIrmaos);


  }
  tratarErroAoPesquisarPorTerritorioPai(erro): any {
    this.abrirSnackBar("Ops... " + erro.error, null, "red-snackbar")
    return [];
  }
  abrirSnackBar(message: string, action: string, classe: string) {
    this.matSnackBar.open(message, action, {
      duration: 3000,
      panelClass: [classe]
    });
  }
}
