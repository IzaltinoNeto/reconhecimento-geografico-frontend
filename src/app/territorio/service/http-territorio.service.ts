import { Page } from '../../model/Page';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import Territorio from 'src/app/model/territorio';
@Injectable({
    providedIn: 'root'
})
export class HttpTerritorioService extends HttpGenerico<Territorio> {

    constructor(http: HttpClient) {
        super(http, '/territorio')
    }

    pesquisarComFiltro(territorioPai: Territorio,nome:string, sort: string, order: string, page: number, size: number): Observable<Page<Territorio>> {
        if (typeof nome !== 'string') {
            nome = "";
        }

        const href = this.urlPadrao + this.urlApi + '/byNameWithPagination';
        const requestUrl =
            `${href}?nome=${nome}&page=${page}&size=${size}`+ (territorioPai ? `&territorioPaiId=${territorioPai.id}` : "");
        return this.http.get(requestUrl)
            .pipe(
                map((res: Page<Territorio>) => res)
            );
    }
   
    pesquisarPorTerritorioPai(id : string, territorioPai: Territorio): Observable<Territorio[]> {
        if(!id)
            id = '0';
        const href = this.urlPadrao + this.urlApi + `/byTerritorioPai`;
        const requestUrl =
            `${href}?territorioPaiId=${territorioPai.id}&territorioId=${id}`;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Territorio[]) => res)
            );
    }
    getHierarquia(): Observable<Territorio[]> {
        const href = this.urlPadrao + this.urlApi + `/getHierarchy`;
        const requestUrl = href;
        return this.http.get(requestUrl)
            .pipe(
                map((res: Territorio[]) => res)
            );
    }


}
